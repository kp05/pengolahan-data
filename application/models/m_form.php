<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class m_form extends CI_Model{
  public function inputform(){

    $data = [
				'tgl' => $this->input->post('tgl_control',true),
				'jam' => $this->input->post('jam_control',true),
				'sector' => $this->input->post('sector',true),
				'kategori' => $this->input->post('kategori',true),
        'mA' => $this->input->post('mA',true),
        'A' => $this->input->post('A',true),
        'fwd' => $this->input->post('fwd',true),
        'rfl' => $this->input->post('rfl',true),
        'ket' => $this->input->post('keter',true)
			];

      $this->db->insert('pemancar',$data);
  }

  public function tampilform(){
      $this->db->order_by('tgl', 'ASC');
      $this->db->order_by('jam', 'ASC');
      $query = $this->db->get('pemancar');
      return $query->result();
  }

  public function deleteform($id){
      $this->db->delete('pemancar', array('no' => $id));
  }

  public function getOneList($id){
      $query = $this->db->select('*');
      $query = $this->db->where('no', $id);
      $query = $this->db->get('pemancar');
      return $query->result();
  }

  public function editform($id){
    $data = [
        'tgl' => $this->input->post('tgl_control',true),
        'jam' => $this->input->post('jam_control',true),
        'sector' => $this->input->post('sector',true),
        'kategori' => $this->input->post('kategori',true),
        'mA' => $this->input->post('mA',true),
        'A' => $this->input->post('A',true),
        'fwd' => $this->input->post('fwd',true),
        'rfl' => $this->input->post('rfl',true),
        'ket' => $this->input->post('keter',true)
      ];


      $this->db->update('pemancar', $data, array('no' => $id));
  }

  public function searchthings($key1,$key2){
    $query = $this->db->select('*');
    $query = $this->db->where('sector',$key1);
    $query = $this->db->where('kategori',$key2);
    $query = $this->db->get('pemancar');
    return $query->result();
  }

  public function searchinput($keyword,$keyword2){
    $query = $this->db->select('*');
    $query = $this->db->where('DATE(tgl) >=', $keyword);
    $query = $this->db->where('DATE(tgl) <=', $keyword2);
    $this->db->order_by('tgl', 'ASC');
    $this->db->order_by('jam', 'ASC');
    $query = $this->db->get('pemancar');
    return $query->result();

  }

}
