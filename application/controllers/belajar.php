<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Belajar extends CI_Controller {

	function __construct(){
		parent::__construct();
      $this->load->library('form_validation');
      $this->load->model('m_form');
	}

	public function index(){
		$this->form_validation->set_rules('username', 'Username', 'required', array('required' => 'Invalid login credentials'));
		$this->form_validation->set_rules('password', 'Password', 'required', array('required' => 'Invalid login credentials'));
		$user = $this->input->post('username');
		$pass = $this->input->post('password');
		if ($this->form_validation->run() == FALSE) {
				$this->load->view('v4_login');
		} else {
			if (($user == 'rrimadiun') AND ($pass == 'indonesia')) {
				redirect('belajar/mainmenu');
			} else {
				$this->session->set_flashdata('flash','Username atau Password Salah');
				$this->load->view('v4_login');
			}
		}
	}

	public function mainmenu(){
    $this->load->view('page_header');
		$this->load->view('v_home');
		$this->load->view('page_footer');
	}

	public function form(){
		$this->load->view('page_header');
		$this->load->view('v_form');
		$this->load->view('page_footer');
	}

  public function input(){
		$this->form_validation->set_rules('sector', 'Sector', 'required');
    if ($this->form_validation->run() == FALSE) {
      $this->load->view('v_form');
			$this->load->view('page_footer');
    } else {
      $this->m_form->inputform();
			$this->load->view('page_header');
			$this->load->view('v_form');
			$this->load->view('page_footer');
    }
  }
	public function list(){
		$this->load->view('page_header');
		$data_list = $this->m_form->tampilform();
		$this->load->view('v_list',['data'=>$data_list]);
		$this->load->view('page_footer');
	}

	public function hapus($id){
		$data_list = $this->m_form->deleteform($id);
		redirect('belajar/list');
	}

	public function edit($id){
		$this->form_validation->set_rules('sector', 'Sector', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data_list = $this->m_form->getOneList($id);
			$this->load->view('v_editform',['data'=>$data_list]);
		} else {
			$this->m_form->editform($id);
			redirect('belajar/list');
		}
	}

	public function search($pro,$am){
		$pro = str_replace("x"," ","$pro");
		$data_list = $this->m_form->searchthings($pro,$am);
		$this->load->view('page_header');
		$this->load->view('v_list',['data'=>$data_list]);
		$this->load->view('page_footer');
	}

	public function search2($pro,$am){
		$data_list = $this->m_form->searchthings($pro,$am);
		$this->load->view('page_header');
		$this->load->view('v_list',['data'=>$data_list]);
		$this->load->view('page_footer');
	}

 	public function about(){
 		$this->load->view('page_header');
 		$this->load->view('v_tentang');
		$this->load->view('page_footer');
 	}

	public function searchbyinput(){
		$keyword = $this->input->post('keyword');
		$keyword2 = $this->input->post('keyword2');
		$data_list = $this->m_form->searchinput($keyword,$keyword2);
		$this->load->view('page_header');
		$this->load->view('v_list',['data'=>$data_list]);
		$this->load->view('page_footer');
	}
}
