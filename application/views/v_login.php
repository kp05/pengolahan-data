<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title></title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            text-decoration: none;
        }
        .bodyadm{
            font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';
            overflow: hidden;
            background-image:  url("<?php echo base_url(); ?>/Assets/bg_login.png");
            background-repeat: no-repeat;

            background-size: cover;
        }

        .box{
            margin-top: 20vh;
            margin-left: 65%;
            font-weight: 300px;
        }

        .texth1{
            font-size: 45px;
            color: white;
            font-weight: 500;
        }
        .textp{
            font-size: 16px;
            color: #ec4638;
            font-weight: 300;
        }
        .texta{
            color: #ec4638;
            font-weight: 700;
        }
        .texta{
            color: #7f91a1;
            font-weight: 700;
        }
        .texta:hover{
            color: #ec4638;
        }
        .formlogin{
            background: transparent;
            color: #ec4638;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
            width: 250px;
        }
        input{
            margin: 20px 0;
            padding: 10px;
            background: transparent;
            border: none;
            outline: none;
            color: #7f91a1;
            font-weight: 500;
            font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';
        }
        button{
            margin: 20px 0;
            padding: 10px;
            background-color: transparent;
            border: none;
            border: 2px solid white;
            color: white;
            border-radius: 20px;
            font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';
            font-size: 16px;
        }
        button:hover{
            border: 2px solid #ec4638;
            background: #ec4638;
            color: #fff;
            cursor: pointer;
        }
        .email, .password{
            border-bottom: 1px solid white;
        }
        .email:focus{
            border-bottom: 1px solid #ec4638;
        }

        .password:focus{
            border-bottom: 1px solid #ec4638;
        }
    </style>
</head>

<body class="bodyadm">
    <main>

        <div class="background">
            <div class="box">
                <h1 class="texth1">Sign In</h1>
                <br>
                <?php if ($this->session->flashdata('flash')) : ?>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <div class="alert alert-info" role="alert">
                                <?= $this->session->flashdata('flash'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                <form class="formlogin" method = 'post' action='<?php echo base_url('belajar') ?>'>
                    <input type="text" class="email" name="username" placeholder="Username" value="">

                    <input type="password" class="password" name="password" placeholder="Password" >

                    <button type="submit" value="Sign In" class="button" >Sign In</button>
                </form>
            </div>
        </div>
    </main>
</body>
</html>
