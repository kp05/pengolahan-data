<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<title>Cara Membuat Viewz</title>
</head>
<body>
			<div class="card mt-5">
                <div class="card-header text-center">
                    <strong>REKAP DATA</strong>
                </div>
                <div class="card-body">
                    
                    <form method="post" action="">
 
            
                        <div class="form-group">
                            <label>Tanggal Control</label>
                            <input type="date" name="tgl_control" placeholder="">
                        </div>

                        <div class="form-group">
                            <label>Jam Control</label>
                            <input type="time" name="jam_control" placeholder="">
                        </div>

                         <div class="dropwdown">
                          <label>Sector Radio</label>
                          <select class="form-control" name="sector" placeholder="Pilih Sector Radio">
                                <option>Radio Pro 1</option>
                                <option>Radio Pro 2</option>
                                <option>Radio Pro 3</option>

                          </select>
                         </div>

                        <div class="form-group">
                            <label>Current mA dan A</label>
                            <input name="grid_plate" class="form-control" placeholder="Masukkan mA dan A">
                        </div>

                        <div class="form-group">
                            <label>Current FWD</label>
                            <input name="fwd" class="form-control" placeholder="Masukkan FWD">
                        </div>

                        <div class="form-group">
                            <label>Current RFL</label>
                            <input name="rfl" class="form-control" placeholder="Masukkan RFL">
                        </div>


                        <div class="form-group">
                            <label>Keterangan</label>
                            <input name="keter" class="form-control" placeholder="Masukkan Keterangan">
                        </div>


                      

                        
 						
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan" style="margin-top: 50px;">
                        </div>
 
                    </form>
 
                </div>
            </div>
</body>
</html>