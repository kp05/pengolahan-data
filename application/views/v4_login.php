<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login - RRI Madiun</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url('assets/logincantik/images/icons/favicon.ico') ?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/vendor/bootstrap/css/bootstrap.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/fonts/iconic/css/material-design-iconic-font.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/vendor/animate/animate.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/vendor/css-hamburgers/hamburgers.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/vendor/animsition/css/animsition.min.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/vendor/select2/select2.min.css') ?>">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/css/util.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/logincantik/css/main.css') ?>">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('<?php echo base_url(); ?>assets/fe2020.jpg');">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
				<form class="login100-form validate-form" method='post' action='<?php echo base_url('belajar') ?>'>
					<span class="login100-form-title p-b-49">
						 Login
					</span>

					<?php if ($this->session->flashdata('flash')) : ?>
						<div class="text-center p-t-8 p-b-31" style=color:red;>
											<div class="alert-validate" role="alert">
													<?= $this->session->flashdata('flash'); ?>
											</div>
						</div>
							<?php endif; ?>

					<div class="wrap-input100 validate-input m-b-23" data-validate = "Username masih kosong">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" name="username" placeholder="Masukkan Username">
						<span class="focus-input100" data-symbol="&#xf206;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Password masih kosong">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" name="password" placeholder="MasukkanPassword">
						<span class="focus-input100" data-symbol="&#xf190;"></span>
					</div>

					<div class="text-right p-t-8 p-b-31">
						<a href="#">

						</a>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button type="submit" class="login100-form-btn">
								Login
							</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincantik/vendor/jquery/jquery-3.2.1.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincantik/vendor/animsition/js/animsition.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincantik/vendor/bootstrap/js/popper.js') ?>"></script>
	<script src="<?php echo base_url('assets/logincantik/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincantik/vendor/select2/select2.min.js') ?>"></script>
<!--===============================================================================================-->


<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincantik/vendor/countdowntime/countdowntime.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/logincantik/js/main.js') ?>"></script>

</body>
</html>
