<!DOCTYPE html>
  <html lang="" dir="ltr">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>">
      <script src="<?php echo base_url('assets/js/jquery.min.js') ?>"></script>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.dataTables.css') ?>">
      <script type="text/javascript" charset="utf8" src="<?php echo base_url('assets/js/jquery.dataTables.js') ?>"></script>
      <script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
      <script src="<?php echo base_url('assets/js/all.min.js') ?>"></script>
      <link rel="stylesheet" href="<?php echo base_url('assets/css/all.min.css') ?>" >
      <title>List Data - RRI Madiun</title>
      <style>
        .bodiadm{
          margin-top: 90px;
        }
        .button_tambah{
    			background-color: #FFFFFF;
    			color: #ec4638;
    			border: 2px solid #ec4638;
    			padding: 5px 10px;
    			margin: 0px 0px 4px 0px;
    			font-size: 15px;
    			border-radius: 25px;
    		}
    		.button_tambah:hover{
    			background-color: #ec4638;
    			color: #FFFFFF;
    		}
      </style>
    </head>
    <body class="bodiadm">
      <div class="container">
        <div class="box">
        <?php $pro1="RadioxProx1"; $pro2="RadioxProx2"; $pro3="RadioxProx3"; $am="AM"; $fm="FM"; ?>
          <center><h3>Rekap Data Pemancar</h3></center>
          <br>
          <div class="form-group">
                <a  class="btn btn-warning"  href="<?php echo base_url('belajar/list') ?>" style="margin-top: 50px;">Show All</a>
                <a  class="btn btn-success"  href="<?php echo base_url('belajar/search/'.$pro1.'/'.$am); ?>" style="margin-top: 50px;">Pro 1 AM</a>
                <a  class="btn btn-success"  href="<?php echo base_url('belajar/search/'.$pro2.'/'.$am) ?>" style="margin-top: 50px;">Pro 2 AM</a>
                <a  class="btn btn-success"  href="<?php echo base_url('belajar/search/'.$pro3.'/'.$am) ?>"  style="margin-top: 50px;">Pro 3 AM</a>
                <a  class="btn btn-success"  href="<?php echo base_url('belajar/search/'.$pro1.'/'.$fm) ?>"  style="margin-top: 50px;">Pro 1 FM</a>
                <a  class="btn btn-success"  href="<?php echo base_url('belajar/search/'.$pro2.'/'.$fm) ?>"  style="margin-top: 50px;">Pro 2 FM</a>
                <a  class="btn btn-success"  href="<?php echo base_url('belajar/search/'.$pro3.'/'.$fm) ?>"  style="margin-top: 50px;">Pro 3 FM</a>
          </div>
          </center>
          <br>
          <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
            <form class="form-inline" method="POST" action="<?php echo base_url('belajar/searchbyinput')?>">
              <input class="form-control mr-sm-2" name="keyword" type="date" placeholder="Search" aria-label="Search"><span> - <span>
              <input class="form-control mr-sm-2" name="keyword2" type="date" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
          </nav>
          <table class="table table-bordered" id="table">
            <thead>
              <tr>
                <th>Hari</th>
                <th>Tanggal Control</th>
                <th>Jam Control</th>
                <th>Sector Radio</th>
                <th>Kategori Radio</th>
                <th>mA</th>
                <th>A</th>
                <th>FWD</th>
                <th>RFL</th>
                <th>Keterangan</th>
                <th>Edit</th>
                <th>Hapus</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($data as $d) { ?>
              <tr>
                <form>
                  <td><?php
                          setlocale(LC_ALL, 'id-ID', 'id_ID');
                          echo strftime("%A", strtotime($d->tgl));

                      ?>
                  </td>
                  <td><?php
                          echo strftime("%d %B %Y", strtotime($d->tgl));
                      ?>
                  </td>
                  <td><?php echo $d->jam ?></td>
                  <td><?php echo $d->sector ?></td>
                  <td><?php echo $d->kategori ?></td>
                  <td><?php echo $d->mA ?></td>
                  <td><?php echo $d->A ?></td>
                  <td><?php echo $d->fwd ?></td>
                  <td><?php echo $d->rfl ?></td>
                  <td><?php echo $d->ket ?></td>

                  <td><a href=<?php echo base_url('belajar/edit/'.$d->no) ?> type="button" class="btn btn-warning" onClick="" ><i class="fas fa-user-times"></i></a></td>
                  <td><a href=<?php echo base_url('belajar/hapus/'.$d->no) ?> type="button" class="btn btn-danger" onClick="return confirm('Apakah Anda Yakin?'" ><i class="fas fa-user-times"></i></a></td>
                </form>
                </tr>
                  <?php } ?>
              </tbody>

          </table>

        </div>
      </div>
    </body>
  </html>
