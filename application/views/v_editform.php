<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
	<title>Cara Membuat Viewz</title>
</head>
<body>
			<div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Edit Data</strong>
                </div>
                <div class="card-body">

                    <form method="post" action="">
                    <?php foreach($data as $d) { ?>

                        <div class="form-group">
                            <label>Tanggal Control</label>
                            <input type="date" name="tgl_control" placeholder="" value=<?php echo $d->tgl ?>>
                        </div>

                        <div class="form-group">
                            <label>Jam Control</label>
                            <input type="time" name="jam_control" placeholder="" value=<?php echo $d->jam ?>>
                        </div>

                         <div class="dropwdown">
                          <label>Sector Radio</label>
                          <select class="form-control" name="sector" placeholder="Pilih Sector Radio" value=<?php echo $d->sector ?>>
                                <option <?php echo ($d->sector == 'Radio Pro 1' ? 'selected' : ''); ?>>Radio Pro 1</option>
                                <option <?php echo ($d->sector == 'Radio Pro 2' ? 'selected' : ''); ?>>Radio Pro 2</option>
                                <option <?php echo ($d->sector == 'Radio Pro 3' ? 'selected' : ''); ?>>Radio Pro 3</option>


                          </select>
                         </div>

                         <div class="dropwdown">
                          <label>Kategori Radio</label>
                          <select class="form-control" name="kategori" placeholder="Pilih Sector Radio" value=<?php echo $d->kategori ?>>
                                <option <?php echo ($d->kategori == 'FM' ? 'selected' : ''); ?>>FM</option>
                                <option <?php echo ($d->kategori == 'AM' ? 'selected' : ''); ?>>AM</option>
                          </select>
                         </div>

                        <div class="form-group">
                            <label>Current mA</label>
                            <input name="mA" class="form-control" placeholder="Masukkan mA" value=<?php echo $d->mA ?>>
                        </div>

                        <div class="form-group">
                            <label>Current A</label>
                            <input name="A" class="form-control" placeholder="Masukkan mA dan A" value=<?php echo $d->A ?>>
                        </div>

                        <div class="form-group">
                            <label>Current FWD</label>
                            <input name="fwd" class="form-control" placeholder="Masukkan FWD" value=<?php echo $d->fwd ?>>
                        </div>

                        <div class="form-group">
                            <label>Current RFL</label>
                            <input name="rfl" class="form-control" placeholder="Masukkan RFL" value=<?php echo $d->rfl ?>>
                        </div>


                        <div class="form-group">
                            <label>Keterangan</label>
                            <input name="keter" class="form-control" placeholder="Masukkan Keterangan" value=<?php echo $d->ket ?>>
                        </div>






                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan" style="margin-top: 50px;">
                        </div>
                    <?php }?>
                    </form>

                </div>
            </div>
</body>
</html>
